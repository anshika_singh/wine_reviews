This project is about the exploratory analysis of wine reviews by different customers in different countries. I have analysed which country is the best and worst wine producer, 
where are the costliest and cheapest wines made, does the most reviewed wineries have the best wines, explotation of most common words used.
In this project i have used the following libraries :
tidyverse, knitr, ggmap, tidytext, tm and readr.
